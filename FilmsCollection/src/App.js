import React from 'react';
import { Provider } from 'react-redux'
import { Provider as PaperProvider } from 'react-native-paper';
import store from './store/store'
import Dashboard from './containers/Dashboard'

function App() {
  return (
      <Provider store={store}>
        <PaperProvider>
          <Dashboard />
        </PaperProvider>
      </Provider>
  )
}

export default App;