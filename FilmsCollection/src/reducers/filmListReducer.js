
import {
    SET_ADD_FILM_TO_LIST,
    SET_REMOVE_FILM_TO_LIST,
    SET_FILM_LIST
} from '../constants/actionTypes';

const initialState = {
    filmListObject: []
}

export default function (state=initialState, action) {
    switch (action.type) {
        case SET_FILM_LIST:
            return {
                ...state,
                ...action.filmList,
                filmListObject: action.filmList
            }
        case SET_ADD_FILM_TO_LIST:
            return state.push(action.film);
        case SET_REMOVE_FILM_TO_LIST:
            return {
                ...state,
                ...action.film
            }
        default:
            return {
                ...state,
            };
    }
}

