import {
    SET_FILM_LIST
} from '../constants/actionTypes';
import axios from 'axios';


export const getFilmsSuccess = (films) => ({
    type: SET_FILM_LIST,
    filmList: films
})

export function getFilmsAction() {
    return (dispatch) => {
        axios.get(`http://192.168.1.130:8010/filmservices`, null,  null )
            .then(response => {
                dispatch(getFilmsSuccess(response.data))
            }).catch(error => {
                console.log('Error al obtenr lista de films');
        })
    }
}

export function getFilmsFilteredAction(filter) {
    return (dispatch) => {
        axios.post(`http://192.168.1.130:8010/filmservices/filter`, {filter:filter},  null )
            .then(response => {
                dispatch(getFilmsSuccess(response.data))
            }).catch(error => {
                console.log('Error al obtenr lista de films');
        })
    }
}




