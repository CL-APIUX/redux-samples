import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import { connect } from 'react-redux';
import {TableContainer, Grid, Table, TableRow, TableHead, TableBody, TableCell, Paper, Button} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {getFilmsAction} from '../actions/filmServiceActions'
import editar from '../assets/editar.png'
import Rating from '@material-ui/lab/Rating';
import EditFilm from './EditFilm';
import { setFilmData } from '../actions/filmDataActions';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const FilmList = () => {

    const classes = useStyles();
    const dispatch = useDispatch();

    const [editing, setEditing] = useState(false);
    const [filmSelected, setFilmSelected] = useState();

    let filmList= useSelector(state => state.filmList.filmListObject);
    let editText = 'Ficha de la película';

    const handleEdit = (value) => {
        if(value!==0){
            editText = 'Editar datos de la película';
            setFilmSelected(filmList.filter((element) => element.id === value)[0]);
        }else{
            editText = 'Ficha de la película';
            setFilmSelected(null);
        }
        setEditing(true);
    } 

    const handleRating = (value, id) => {
        console.log('CTL: handleRating => value: ', value);
        console.log('CTL: handleRating => id: ', id);
    } 

    const onChangeEdit = (value, callback) => {
        console.log('CTL: onChangeEdit => : value', value);
        callback(value);
    } 

    useEffect(() => {
        const objFilmData = {
            editing,
            filmSelected
        }
        dispatch(setFilmData(objFilmData))
    }, [editing]);

    useEffect(() => {
        dispatch(getFilmsAction())
    },[]);

    useEffect(() => {
        if(!!(filmList) && filmList.length>0){
            console.log("CTL: Llego la lista de films:", filmList);
        }
    }, [filmList]);

    const renderFilms = () => {
        const filmsForRender = !!(filmList) ? filmList.map ( element => {
            return  (
                <TableRow>
                    <TableCell>{element.film}</TableCell>    
                    <TableCell>{element.year}</TableCell>    
                    <TableCell>{element.genre}</TableCell>    
                    <TableCell>{element.leadStudio}</TableCell>    
                    <TableCell>{element.audienceScore}</TableCell>    
                    <TableCell>{element.profitability}</TableCell>    
                    <TableCell>
                    <Rating
                        value={element.rating}
                        onChange={(event, newValue) => {
                            handleRating(newValue, element.id);
                        }}
                    />                        
                    </TableCell>    
                    <TableCell>
                        <Button  style={{marginRight: -20}} onClick={() => { handleEdit(element.id) }} >
                            <img src={editar} width="20" height="20" />
                        </Button>
                    </TableCell>    
                </TableRow>                        
            );
        }) : null;
        return filmsForRender;
    }

    const render = () =>{
        if(!editing){
            return(
                <div className={classes.root}>
                <Grid container spacing={3} className={classes.root_inputs}  direction="column" alignItems="center" justify="center">
                    <Grid item xs={12}>
                        <Button variant="contained" color="secondary" onClick={() => { handleEdit(0)}} >
                            Nueva película
                        </Button>

                    </Grid>
                </Grid>
                <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Film</TableCell>    
                            <TableCell>Año</TableCell>    
                            <TableCell>Genero</TableCell>    
                            <TableCell>Studio</TableCell>    
                            <TableCell>Calif.Aud. %</TableCell>    
                            <TableCell>Beneficios</TableCell>    
                            <TableCell>Mi opinión</TableCell>    
                            <TableCell>Acciones</TableCell>    
                        </TableRow>                        
                    </TableHead>
                    <TableBody>
                        {renderFilms()}
                    </TableBody>
                </Table>
                </TableContainer>
                </div>
            );
        }else{   
            return(
                <EditFilm callback={setEditing} changeValue={onChangeEdit} data={filmSelected} textHeader={editText} dispatch={dispatch} />
            )     
        }
    }

    return (
        <div className={classes.root}>
            {render()}
        </div>
    );
    
}


export default connect()(FilmList);