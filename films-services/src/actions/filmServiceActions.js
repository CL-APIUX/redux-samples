import axios from 'axios';
import {GET_FILMS_SERVICE, GLOBAL_URL_API} from '../constants/serviceConstants'
import {
    SET_FILM_LIST,
    SET_ADD_FILM_TO_LIST
} from '../constants/actionTypes';

export const getFilmsSuccess = (films) => ({
    type: SET_FILM_LIST,
    filmList: films
})

export const addFilmSuccess = (film) => ({
    type: SET_ADD_FILM_TO_LIST,
    film: film
})

export function getFilmsAction() {
    return (dispatch) => {
        axios.get(`${GLOBAL_URL_API}${GET_FILMS_SERVICE}`, null,  null )
            .then(response => {
                dispatch(getFilmsSuccess(response.data))
            }).catch(error => {
                console.log('Error al obtenr lista de films');
        })
    }
}

export function addFilmAction(body) {
    return (dispatch) => {
        axios.post(`${GLOBAL_URL_API}${GET_FILMS_SERVICE}`, body,  null )
            .then(response => {
                dispatch(addFilmSuccess(response.data))
            }).catch(error => {
                console.log('Error al agregar un film');
        })
    }
}


