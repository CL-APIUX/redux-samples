import React from 'react';
import { connect } from 'react-redux';
import {Grid} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import FilmList from '../components/FilmList'
import FilmsHeader from './FilmsHeader'
import LeftPanel from './LeftPanel'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const DashBoard = () => {
    
    return (
        <div className={useStyles.root}>
            <Grid >
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <FilmsHeader/>
                    </Grid>
                    <Grid item xs={3} >
                        <LeftPanel/>
                    </Grid>
                    <Grid item xs={8}>
                        <FilmList />
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );

}


export default connect()(DashBoard);