import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SaveIcon from '@material-ui/icons/Save';
import Badge from '@material-ui/core/Badge';
import { useSelector, useDispatch } from 'react-redux';
import {addFilmAction} from '../actions/filmServiceActions'



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  barStyle: {
    backgroundColor: '#FFF',
    color: '#000'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function FilmsHeader() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const filmData = useSelector(state => state.filmData);

  const handleSave = () => {
    const body = {
      id: filmData.filmData.id,
      film: filmData.filmData.film,
      genre: filmData.filmData.genre,
      leadStudio: filmData.filmData.leadStudio,
      audienceScore: filmData.filmData.audienceScore,
      profitability: filmData.filmData.profitability,
      rottenTomatoes: filmData.filmData.rottenTomatoes,
      rating: filmData.filmData.rating,
      worldwideGross: filmData.filmData.worldwideGidross,
      year: filmData.filmData.year
    };
    dispatch(addFilmAction(body));
  } 


  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.barStyle} >
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Adictos a las películas
          </Typography>
          <IconButton aria-label="show 4 new mails" color="inherit" onClick={handleSave} >
              <Badge color="secondary">
                <SaveIcon />
              </Badge>
          </IconButton>


        </Toolbar>
      </AppBar>
    </div>
  );
}
