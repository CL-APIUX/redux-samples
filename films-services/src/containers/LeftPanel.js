import React , { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import films from '../assets/films.png';
import { Paper } from '@material-ui/core';

const useStyles = makeStyles({
root: {
    marginBottom: '30px'
},

media: {
    marginTop: '15px',
    margin: "-70px auto 0",
    width: "90%",
    height: 240,
    borderRadius: "4px",
    boxShadow: "0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)",
    position: "relative",
    zIndex: 1000
  }
});

export default function LeftPanel() {
  const classes = useStyles();

  const [filmsNumber, setFilmsNumber] = useState(0);
  const [usersNumber, setUsersNumber] = useState(0);


  return (
    <Grid >
    <Grid container spacing={2} >
        <Grid item xs={12}>
            <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                component="img"
                className={classes.media}
                image={films}
                />
                <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    Locos por el cine
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    Esta sitio está dedicado a los amantes del cine, para poder mantener información 
                    actualizada sobre las películas y sus datos y poder compartir la opinión de nuestra comunidad
                </Typography>
                </CardContent>
            </CardActionArea>
            </Card>
            <Card className={classes.root}>
            <CardActionArea>
                <CardContent>
                <Typography gutterBottom variant="h5" component="h2" className={classes.root}>
                    Datos sobre nuestro sitio
                </Typography>
                <Grid className={classes.root} >
                    <Grid container spacing={2} >
                        <Grid item xs={6}>
                            <TextField
                                label="Número de películas en el catálogo"
                                value={filmsNumber}
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Número de usuarios"
                                value={usersNumber}
                                variant="outlined"
                                InputProps={{
                                    readOnly: true,
                                }} />
                        </Grid>
                    </Grid>
                </Grid>
                </CardContent>
            </CardActionArea>
            </Card>
        </Grid>
    </Grid>
</Grid>

  );
}
