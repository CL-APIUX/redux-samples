import { makeStyles } from '@material-ui/core/styles';

const colorGreenLight = 'rgba(143, 190, 0, 0.20000000298023224)';
const colorGreen = '#8FBE00';
const white = '#fff';
const black = '#000';
const font = "'Lato', sans-serif";

const useStyles = makeStyles(theme => ({
  
    root_nav: {
      /* flexGrow: 1, */
      '& .MuiAppBar-root':{
        borderTopLeftRadius: '10px',
        borderTopRightRadius: '10px',
        borderBottomLeftRadius: '5px',
        borderBottomRightRadius: '5px',
      },

    },
    root_appBar: {
      backgroundColor: white,
      color: black,
      boxShadow: 'none',
    },
    root_tab: {
      minWidth: '100px',
      height: '35px',
      border: 'solid 1px #C1C1C1',
    },
    root_tabs: {
      '& .MuiTabs-indicator' : {
        display: 'none',
      },
      '& .MuiTab-root':{
        fontFamily: font,
        fontSize: '14px',
        fontWeight: '900',
        letterSpacing: '0.2px',
        lineHeight: '17px',
        textAlign: 'left',
        textTransform: 'capitalize'
      }
    },
    root_containerPanel: {
      width: '100%',
      marginTop: '20px',
    },
    expansion_panel:{
      marginBottom: '20px',
      borderRadius: '8px',
      '& .MuiExpansionPanel-root:before':{
        height: '0'
      }
    },
    root_inputs: {
      '& .MuiInputBase-input' : {
        height: '40px',
      },
      '& .MuiOutlinedInput-input' :{
        padding: '0px 14px',
      },
      '& .MuiTextField-root': {
        width: '100%',
      },
      '& .MuiFormLabel-root.Mui-focused' : {
        color: colorGreen,
        transform: 'translate(14px, -6px) scale(0.75)',
      },
      '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
        transform: 'translate(14px, -6px) scale(0.75)',
      },
      '& .MuiInputLabel-outlined': {
        transform: 'translate(14px, 12px) scale(1)',
      },
      '& .MuiFormLabel-root': {
        fontFamily: font,
        fontSize: '16px',
      },
      '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline' : {
        borderColor: colorGreen,
      },
      '& .MuiOutlinedInput-multiline':{
        padding: '0',
        minHeight: '40px',
      },
     '& .MuiAutocomplete-inputRoot': {
       padding: '0',
       //height: '40px',
       '& .MuiAutocomplete-input': {
         padding: '0 !important',
         transform: 'translate(0px, -9px)',
         marginLeft: '15px',
         marginTop: '10px',
         marginBottom: '-10px',
       },
       '& .MuiAutocomplete-tag': {
        margin:10,
      }
      },
      alignSelf: 'center',
    },
    root_paperContainer: {
      padding: '12px',
      border: '1px solid #C1C1C1',
      backgroundColor: 'transparent',
      borderRadius: '8px',
      marginBottom: '20px',
      '& .MuiExpansionPanel-root:before': {
        height: '0',
      },
      '& .MuiExpansionPanel-rounded:first-child': {
        borderTopRightRadius: '10px',
        borderTopLeftRadius: '10px',
      },
      '& .MuiExpansionPanel-rounded': {
        borderRadius: '10px',
      },
      '& .MuiExpansionPanel-rounded:last-child': {
        borderBottomRightRadius: '10px',
        borderBottomLeftRadius: '10px',
      },
      '& .MuiExpansionPanelSummary-expandIcon': {
        transform: 'rotate(0deg) scale(1.5)',
      },
      '& .MuiExpansionPanelSummary-expandIcon.Mui-expanded': {
        color: colorGreen,
        transform: 'rotate(180deg) scale(1.5)',
      }
    },
    root_paperContainer_panel:{
      backgroundColor: '#FFFFFF',
      boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.3)',
      borderRadius: '15px',
      marginBottom: '20px',
    },
    root_panelSummary: {
      '& .MuiExpansionPanel-rounded:first-child':{
        borderRadius: '0px'
      },
      '& .MuiExpansionPanel-rounded':{
        borderRadius: '10px',
      },
    },
    root_divider: {
      '& .MuiDivider-root': {
        margin: '8px',
      },
    },
    root_cardBox:{
      marginTop: '80px',
      backgroundColor: '#FFFFFF !important',
      borderRadius: '0 8px 8px 0 !important',
      boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.3) !important',
    },
    root_cardContent:{
      padding: '10px 16px !important'
    },
    root_cardInfo:{
      borderRadius: '5px',
      width: '90%',
      border: '1px solid #C1C1C1',
      boxShadow:'none',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginBottom: '10px',
      marginTop: '10px',
    },
    root_cardHeader :{
      padding: '7px !important',
      backgroundColor: colorGreenLight,
      textAlign: 'center',
    },
    root_cardTitle :{
      fontFamily: 'Lato, sans-serif',
      fontSize:'1rem',
      fontWeight: '700',
      letterSpacing: '0.5px',
      paddingLeft: '8px',
    },
    root_cardSubTitle :{
      fontSize:'.8rem',
      fontWeight: '600',
      width: '55%',
      float: 'left',
      marginBottom: '10px',
    },
    root_cardText :{
      fontSize:'.8rem',
      letterSpacing: '.21px',
      textAlign:'right',
      float: 'right',
      width: '45%',
      marginBottom: '10px',
    },
    root_formControl:{
      '& .MuiFormControl-root':{
        width: '100%',
      }
    },
    text: {
      paddingLeft: '12px',
      paddingBottom: '20px',
      paddingTop: '12px',
      color: black,
      fontSize: '14px',
      fontWeight: 'bold',// 400
      letterSpacing: '0.25px',
      lineHeight: '17px',
      textAlign:'left',
      fontFamily: font,
    },
    subtitle_bold: {
      color: black,
      fontSize: '1rem',
      fontWeight: '700',
      letterSpacing: '0.5px',
      lineHeight: '19px',
      textAlign: 'left',
      fontFamily: font,
      marginBottom: '10px',
    },
    subtitle_section: {
      paddingLeft: '12px',
      color: black,
      fontSize: '1rem',
      fontWeight: '700',
      letterSpacing: '0.5px',
      lineHeight: '19px',
      textAlign: 'left',
      fontFamily: font,
    },
    title_sections: {
      paddingLeft: '24px',
      fontSize: '1rem',
      fontWeight: '700',
      fontFamily: font,
      paddingBottom: '8px',
      letterSpacing: '0.11px',
      lineHeight: '19px',
    },
    alignSelf:{
      alignSelf: 'center',
    },
    float_left:{
      float: 'left',
    },
    linear: {
      height: '10px',
      '& .MuiLinearProgress-root': {
        height: '10px',
      },
      '& .MuiLinearProgress-barColorPrimary': {
        backgroundImage: 'linear-gradient(270deg, #E02020 0%, #F7B500 50.09%, #8FBE00 100%)'
      }
    },
    top_bottom_spacing:{
      marginTop: '10px',
      marginBottom: '5px',
    },
    radioDistribuited: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    noBorder:{
      border: 'none'
    },
    noPadding:{
      padding: '0 !important'
    },
    boxButton:{
      marginTop:'15px',
      width: '100%',
      maxWidth: '400px',
      float:'right',
    },
    boxBorder:{
      border: '1px solid #C1C1C1',
      padding: 20
    },
    spacesPadding:{
      padding: 30,
    },
    spacesBottom: {
      marginBottom: 40,
    },
    checkListFont:{
      fontSize: '12px',
    },
    timeListFont:{
      fontSize: '12px',
      color: 'rgba(0, 0, 0, 0.5)',
    },
    numberListFont:{
      fontSize: '10px',
      color: 'rgba(0, 0, 0, 0.5)',
    },
    colorGreen:{
      color: colorGreen,
    },
    marginItems:{
      marginTop:30,
      marginBottom:10,
    },
    lineTitleHr:{
      marginTop: -5
    },
    titleSectionDefinided:{
      fontSize: '16px',
      fontWeight: '700',
      fontFamily: font,
      paddingBottom: '8px',
      letterSpacing: '0.11px',
      lineHeight: '19px',
    },
    subTitleSectionDefinided:{
      fontSize: '14px',
      fontWeight: '700',
      fontFamily: font,
      paddingBottom: '8px',
      letterSpacing: '0.11px',
      lineHeight: '19px',
    },
    boxTimeSpacing:{
      margin: '0 auto', 
      display: 'block', 
      width:'240px'
    },
    boxNumberSpacing:{
      margin: '0 auto', 
      display: 'block', 
      width:'290px'
    },
    paddingExtra: {
      padding: '12px',
      backgroundColor: '#FFFFFF',
      boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.3)',
      borderRadius: '15px',
      marginBottom: '20px',
    },
    paddingExtraRisk: {
      padding: '0px 10px !important',
      backgroundColor: '#FFFFFF',
      boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.3)',
      borderRadius: '15px',
      marginBottom: '20px',
    },
    paddingOrder:{
      paddingLeft: 20,
      paddingRight: 20,
    } 
  
}));

export default useStyles;