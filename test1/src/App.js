
import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';

import store from './store/store'
import Dashboard from './containers/dashboard'

function PrivateRoute({path, component, ...rest}){

    return <Route path={path} component={component} {...rest} />

    //Si tuvieramos que redirigir a la pagina de login en caso de que no estuvieramos autenticados
    // let token = sessionStorage.getItem('token');
    // if(!!(token)){
    //    return <Route path={path} component={component} {...rest} />
    // }
    // return <Redirect to="/fimlsCollection/login" {...rest} />
}


function App(){

    return(
        <Provider store={store}>
            <Switch>
                <PrivateRoute exact path="/filmsCollection/dashboard" component={Dashboard} />
            </Switch>
        </Provider>
    )

}

export default App;

