import { createStore, applyMiddleware, compose, combineReducers  } from 'redux';
import thunk from 'redux-thunk';
import filmList from '../reducers/filmListReducer'

const initialState = {};

const rootReducer = combineReducers({
    filmList
})

const middleware = [thunk];

const logger = store => next => action => {
    console.log('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    return result
}

const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(logger, ...middleware)
);

export default store;







