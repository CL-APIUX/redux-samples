import axios from 'axios';
import {
    SET_FILM_LIST
} from '../constants/actionTypes';

export const getFilmSucess = (films) => ({
    type: SET_FILM_LIST,
    filmList : films
})


export function getFilmsAction(){
    return (dispatch) => {
        axios.get(`http://localhost:8081/filmservices`, null, null)
            .then(response => {
                dispatch(getFilmSucess(response.data))
            }).catch(error => {
                console.log('Error al obtener peliculas');
            })
    }
}
