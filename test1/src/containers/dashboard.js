import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import { connect } from 'react-redux';
import {TableContainer, Table, TableRow, TableHead, TableBody, TableCell, Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {getFilmsAction} from '../actions/filmListActions'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const DashBoard = () => {

    const classes = useStyles();
    const dispatch = useDispatch();

    let filmList = useSelector(state => state.filmList.filmListObject);

    useEffect(() => {
        dispatch(getFilmsAction());
    }, []);

    useEffect(() => {
        if(!!(filmList) && filmList.length>0){
            console.log('CTL: Llego la lista de films:', filmList);
        }
    }, [filmList]);


    const renderFilms = () => {
        const filmsForRender = !!(filmList) ? filmList.map ( element => {
            return (
                <TableRow>
                    <TableCell>{element.film}</TableCell>    
                    <TableCell>{element.year}</TableCell>    
                    <TableCell>{element.genre}</TableCell>    
                    <TableCell>{element.leadStudio}</TableCell>    
                    <TableCell>{element.audicienceScore}</TableCell>    
                    <TableCell>{element.profitability}</TableCell>    
                    <TableCell></TableCell>    
                    <TableCell></TableCell>    
                </TableRow>
            )
        }) : null;
        return filmsForRender;
    }

    return (
        <div className={classes.root}>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Film</TableCell>    
                            <TableCell>Año</TableCell>    
                            <TableCell>Genero</TableCell>    
                            <TableCell>Studio</TableCell>    
                            <TableCell>Calif.Aud. %</TableCell>    
                            <TableCell>Beneficios</TableCell>    
                            <TableCell>Mi opinión</TableCell>    
                            <TableCell>Acciones</TableCell>    
                        </TableRow>                        
                    </TableHead>
                    <TableBody>
                        {renderFilms()}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>

    );

}


export default connect()(DashBoard);







