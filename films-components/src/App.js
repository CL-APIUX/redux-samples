import React from 'react';
import { Provider } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom';
import store from './store/store'
import Dashboard from './containers/Dashboard'

function PrivateRoute({ path, component, ...rest }){
    return <Route path={path} component={component} {...rest} />
    //  Si tuvieramos que redirigir a la página de login en caso de no estar logados
    //  let token = sessionStorage.getItem('token');
    //  if (token) {
    //    return <Route path={path} component={component} {...rest} />
    //  }
    //  return <Redirect to="/filmsCollection" {...rest} />
}


function App() {
  return (
      <Provider store={store}>
        <Switch>
            <PrivateRoute exact path="/filmsCollection/dashboard" component={Dashboard} />
        </Switch>
      </Provider>
  )
}

export default App;
