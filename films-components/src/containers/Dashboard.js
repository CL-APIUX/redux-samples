import React from 'react';
import { connect } from 'react-redux';
import {TableContainer, Table, TableRow, TableHead, TableBody, TableCell, Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const DashBoard = () => {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Film</TableCell>    
                            <TableCell>Año</TableCell>    
                            <TableCell>Genero</TableCell>    
                            <TableCell>Studio</TableCell>    
                            <TableCell>Calif.Aud. %</TableCell>    
                            <TableCell>Beneficios</TableCell>    
                            <TableCell>Mi opinión</TableCell>    
                            <TableCell>Acciones</TableCell>    
                        </TableRow>                        
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>Zack and Miri Make a Porno</TableCell>    
                            <TableCell>2008</TableCell>    
                            <TableCell>Romance</TableCell>    
                            <TableCell>The Weinstein Company</TableCell>    
                            <TableCell>70%</TableCell>    
                            <TableCell>1.747541667</TableCell>    
                            <TableCell>Mi opinión</TableCell>    
                            <TableCell>Acciones</TableCell>    
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
        </div>

    );

}


export default connect()(DashBoard);