import { combineReducers } from 'redux';

import {
    SET_ADD_FILM_TO_LIST,
    SET_REMOVE_FILM_TO_LIST
} from '../constants/actionTypes';


const filmReducer = (state={}, action) => {
    switch (action.type) {
        case SET_ADD_FILM_TO_LIST:
            return {
                ...state,
                ...action.films
            }
        case SET_REMOVE_FILM_TO_LIST:
            return {
                ...state,
                ...action.films
            }
        default:
            return {
                ...state,
            };
    }
}

export default combineReducers({
    films: filmReducer
});



