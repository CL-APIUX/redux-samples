import React from 'react';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';

import store from './store/store'
import Dashboard from './containers/Dashboard'



function PrivateRoute({ path, component, ...rest}){

    return <Route exact path='/filmsCollection/dashboard' component={Dashboard} />
    //let token = sessionStorage.getItem('token');
    //if(!!(token)){
    //    return <Route exact path='/filmsCollection/dashboard' component={Dashboard} />
    //}
    //return <Redirect to='/filmsCollection/login' {...rest} />

}

function App(){
    return (
        <Provider store={store}>
            <Switch>
                <PrivateRoute exat path='/filmsCollection/dashboard' component={Dashboard} />
            </Switch>
        </Provider>
    )
}

export default App;






