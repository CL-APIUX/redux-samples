
import React, { useEffect, useState } from 'react';
import {Box,Grid,Typography,Button, TextField, Select, MenuItem} from '@material-ui/core'
import useStyle from '../styles/GlobalStyles';
import Rating from '@material-ui/lab/Rating';
import { makeStyles } from '@material-ui/core/styles';
import { setFilmData } from '../actions/filmDataActions';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        width: '200px'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));


const EditFilm = ({dispatch}) => {

    const classes = useStyle();
    const localClasess = useStyles();

    const [id, setId] = useState('');
    const [film, setFilm] = useState('');
    const [genre, setGenre] = useState('');
    const [leadStudio, setLeadStudio] = useState('');
    const [audienceScore, setAudienceScore] = useState('');
    const [profitability, setProfitability] = useState('');
    const [rottenTomatoes, setRottenTomatoes] = useState('');
    const [rating, setRating] = useState('');
    const [worldwideGidross, setWorldwideGidross] = useState('');
    const [year, setYear] = useState('');

    const [saveFlag, setSaveFlag] = useState(true);

    useEffect(()=> {
        const objFilmData = {
            id,
            rating
        }
        dispatch(setFilmData(objFilmData));
    }, [saveFlag]);


    const handleDescription = (e, callBack) => {
        callBack(e.target.value);
    }

    const handleRating = (value, callback) => {
        callback(value);
        setSaveFlag(saveFlag ? false : true);
    }

    const handleSave = () => {
        setSaveFlag(saveFlag ? false : true);
    }
    
    const handleSelectors = (e) => {
        setGenre(e.target.value);
    } 

    return (
        <Box>
            <Typography className={classes.titleSectionDefinided}>Ficha de la película</Typography>
            <hr className={classes.lineTitleHr}/>
            <div className={classes.root}>
                <Grid container spacing={3} className={classes.root_inputs} >
                    <Grid item xs={6}>
                        <TextField
                            label='id'
                            disabled={true}
                            value={id}
                            onChange={e => handleDescription(e, setId)}
                            onBlur={handleSave}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Rating
                                value={rating}
                                onChange={(event, newValue) => {
                                    handleRating(newValue, setRating);
                                }}
                            />                        
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Nombre de la película"
                            multiline
                            disabled={false}
                            value={film}
                            onChange={e => handleDescription(e, setFilm)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <Select
                            value={genre}
                            onChange={handleSelectors}
                            label="Genero" 
                            variant="outlined" 
                            className={localClasess.root}>
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value='Romance'>Romance</MenuItem>
                            <MenuItem value='Comedy'>Comedy</MenuItem>
                            <MenuItem value='Animation'>Animation</MenuItem>
                            <MenuItem value='Drama'>Drama</MenuItem>
                        </Select>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Estudio principal"
                            disabled={false}
                            value={leadStudio}
                            onChange={e => handleDescription(e, setLeadStudio)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Calificación de la audiencia"
                            disabled={false}
                            value={audienceScore}
                            onChange={e => handleDescription(e, setAudienceScore)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Beneficios"
                            disabled={false}
                            value={profitability}
                            onChange={e => handleDescription(e, setProfitability)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Tomates podridos"
                            disabled={false}
                            value={rottenTomatoes}
                            onChange={e => handleDescription(e, setRottenTomatoes)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Ingresos a nivel mundial"
                            disabled={false}
                            value={worldwideGidross}
                            onChange={e => handleDescription(e, setWorldwideGidross)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            label="Año de lanzamiento"
                            disabled={false}
                            value={year}
                            onChange={e => handleDescription(e, setYear)}
                            onBlur={handleSave}
                            variant="outlined"
                            />
                    </Grid>
                </Grid>
                <Grid container spacing={3} className={classes.root_inputs}  direction="column" alignItems="center" justify="center">
                    <Grid item xs={12}>
                        <Button variant="contained" color="secondary"  >
                            Volver
                        </Button>

                    </Grid>
                </Grid>
            </div>
        </Box>
    );
}

export default EditFilm;
