import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {TableContainer, Grid, Button, Table, TableRow, TableHead, TableBody, TableCell, Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {useDispatch, useSelector} from "react-redux";
import {getFilmsAction} from '../actions/filmListActions'
import Rating from '@material-ui/lab/Rating';
import editar from '../assets/editar.png'
import EditFilm from './EditFilm';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));


const FilmList = () => {

    const classes = useStyles();
    const dispatch = useDispatch();


    const [editing, setEditing] = useState(false);

    let filmList= useSelector(state => state.filmList.filmListObject);

    const handleEdit = (value) => {
        setEditing(true);
    }

    const handleRating = (value, id) => {
        console.log('CTL: handleRating => value: ', value);
        console.log('CTL: handleRating => id: ', id);
    } 

    useEffect(() => {
        dispatch(getFilmsAction())
    },[]);

    useEffect(() => {
        if(!!(filmList) && filmList.length>0){
            console.log("CTL: Llego la lista de films:", filmList);
        }
    }, [filmList]);

    const renderFilms = () => {
        const filmsForRender = !!(filmList) ? filmList.map ( element => {
            return  (
                <TableRow>
                    <TableCell>{element.film}</TableCell>    
                    <TableCell>{element.year}</TableCell>    
                    <TableCell>{element.genre}</TableCell>    
                    <TableCell>{element.leadStudio}</TableCell>    
                    <TableCell>{element.audienceScore}</TableCell>    
                    <TableCell>{element.profitability}</TableCell>    
                    <TableCell>
                    <Rating
                        value={element.rating}
                        onChange={(event, newValue) => {
                            handleRating(newValue, element.id);
                        }}
                        />                                   
                    </TableCell>    
                    <TableCell>
                        <Button style={{marginRight: -20}} onclick={() => handleEdit(element.id)} >
                            <img src={editar} width="20" height="20" />
                        </Button>
                    </TableCell>    
                </TableRow>                        
            );
        }) : null;
        return filmsForRender;
    }
    
    const render = () => {
        if(!editing){
            return (
                <div className={classes.root}>
                    <Grid container spacing={3} className={classes.root_inputs}  direction="column" alignItems="center" justify="center">
                        <Grid item xs={12}>
                            <Button variant="contained" color="secondary" onClick={() => { handleEdit(0)}} >
                                Nueva película
                            </Button>

                        </Grid>
                    </Grid>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Film</TableCell>    
                                    <TableCell>Año</TableCell>    
                                    <TableCell>Genero</TableCell>    
                                    <TableCell>Studio</TableCell>    
                                    <TableCell>Calif.Aud. %</TableCell>    
                                    <TableCell>Beneficios</TableCell>    
                                    <TableCell>Mi opinión</TableCell>    
                                    <TableCell>Acciones</TableCell>    
                                </TableRow>                        
                            </TableHead>
                            <TableBody>
                                {renderFilms()}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            )
        }else{
            return (
                <EditFilm dispatch={dispatch} />
            )
        }
    }

    return (
        <div className={classes.root}>
            {render()}
        </div>
    );

}

export default connect()(FilmList);



