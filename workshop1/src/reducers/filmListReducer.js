import {
    SET_FILM_LIST,
    SET_ADD_FILM_TO_LIST
} from '../constants/actionTypes'


const initialState = {
    filmListObject : []
}

export default function (state=initialState, action){
    switch (action.type) {
        case SET_FILM_LIST:
            return {
                ...state,
                ...action.filmList,
                filmListObject: action.filmList,
            }
        case SET_ADD_FILM_TO_LIST:
            state.push(action.film)
            return {...state};
        default:
            return {
                ...state,
            };
    }
}



