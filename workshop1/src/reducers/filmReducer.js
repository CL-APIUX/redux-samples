import { combineReducers } from 'redux';

import {
    SET_FILM_DATA,
} from '../constants/actionTypes';

const setFilmDataReducer = (state={}, action) => {
    switch (action.type) {
        case SET_FILM_DATA:
            return {
                ...state,
                ...action.filmData
            }
        default:
            return {
                ...state
            }
    }
}

export default combineReducers({
    filmData : setFilmDataReducer
})
