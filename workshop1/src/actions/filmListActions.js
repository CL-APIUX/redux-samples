import {
    SET_FILM_LIST
} from '../constants/actionTypes';
import axios from 'axios';


export const getFilmsSuccess = (films) => ({
    type: SET_FILM_LIST,
    filmList: films
})

export function getFilmsAction() {
    console.log("CTL: 1");
    return (dispatch) => {
        axios.get(`http://localhost:8081/filmservices`, null,  null )
            .then(response => {
                console.log("CTL: 2");
                dispatch(getFilmsSuccess(response.data))
            }).catch(error => {
                console.log('Error al obtenr lista de films');
        })
    }
}





