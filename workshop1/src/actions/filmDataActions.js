import {
    SET_FILM_DATA,
} from '../constants/actionTypes';

const setFilmData = (data) => ({
    type: SET_FILM_DATA,
    filmData: data,
});

export {
    setFilmData
};



