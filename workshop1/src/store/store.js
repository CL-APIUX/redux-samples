import { createStore, applyMiddleware, compose, combineReducers  } from 'redux';
import thunk from 'redux-thunk';
import filmList from '../reducers/filmListReducer';
import filmData from '../reducers/filmReducer';

const rootReducer = combineReducers({
    filmList,
    filmData
})

const middleware = [thunk];

const initialState = {};

const logger = store => next => action => {
    console.log('dispacthing:', action);
    let result = next(action);
    console.log('next state:', store.getState());
    return result;
}

const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(logger, ...middleware)
);

export default store;




