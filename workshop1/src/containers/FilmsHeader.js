
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import SaveIcon from '@material-ui/icons/Save';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    barStyle: {
      backgroundColor: '#FFF',
      color: '#000'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));
  

export default function FilmsHeader(){

    const classes = useStyles();

    const handleSave = () => {
        console.log('CTL: handleSave');
    }

    return (
    <div className={classes.root}>
        <AppBar position="static" className={classes.barStyle} >
            <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
                Adictos a las películas
            </Typography>
            <IconButton aria-label="show 4 new mails" color="inherit" onClick={handleSave} >
                <Badge color="secondary">
                    <SaveIcon />
                </Badge>
            </IconButton>
            </Toolbar>
        </AppBar>
    </div>
        
    )

}


